package com.gagi.passwd.manager.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistryBuilder;

import com.googlecode.genericdao.dao.hibernate.GeneralDAOImpl;

public class BaseDaoImpl extends GeneralDAOImpl {

	private static final String HIBERNATE_CONFIG = "embeded.hibernate.cfg.xml";
	private Configuration configuration = new Configuration().configure(HIBERNATE_CONFIG);
	private ServiceRegistryBuilder serviceRegistryBuilder = new ServiceRegistryBuilder().applySettings(configuration.getProperties());
	private SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistryBuilder.buildServiceRegistry());
	private Session session = sessionFactory.openSession();
	
	@Override
	protected Session getSession() {
		return this.session;
	}
	
	public void setSession(Session session) {
		this.session = session;
		setSessionFactory(session.getSessionFactory());
	}
}
