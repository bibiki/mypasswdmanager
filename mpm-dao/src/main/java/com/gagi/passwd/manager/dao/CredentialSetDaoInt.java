package com.gagi.passwd.manager.dao;

import java.util.Set;

import com.gagi.passwd.manager.domain.CredentialSet;

public interface CredentialSetDaoInt {

	CredentialSet save(CredentialSet credentialSet);
	
	void delete(CredentialSet credentialSet);
	
	CredentialSet getById(long id);
	
	CredentialSet update(CredentialSet credentialSet);
	
	Set<CredentialSet> getAll();
}
