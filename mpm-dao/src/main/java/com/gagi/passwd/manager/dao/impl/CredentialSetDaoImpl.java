package com.gagi.passwd.manager.dao.impl;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.gagi.passwd.manager.dao.CredentialSetDaoInt;
import com.gagi.passwd.manager.domain.CredentialSet;

public class CredentialSetDaoImpl extends BaseDaoImpl implements CredentialSetDaoInt {

	private Session session = super.getSession();
	
	public CredentialSet save(CredentialSet credentialSet) {
		Transaction tx = session.beginTransaction();
		Long id = (Long)session.save(credentialSet);
		tx.commit();
		credentialSet.setId(id);
		return credentialSet;
	}
	
	public void delete(CredentialSet credentialSet) {
		Transaction tx = session.beginTransaction();
		session.delete(credentialSet);
		tx.commit();
	}
	
	public CredentialSet getById(long id) {
		return (CredentialSet)session.get(CredentialSet.class, id);
	}
	
	public CredentialSet update(CredentialSet credentialSet) {
		Transaction tx = session.beginTransaction();
		session.update(credentialSet);
		tx.commit();
		return this.getById(credentialSet.getId());
	}

	public Set<CredentialSet> getAll() {
		Criteria criteria = session.createCriteria(CredentialSet.class);
		return new HashSet<CredentialSet>(criteria.list());
	}
}
