Purpose
=======
I started this project some time in early 2016 with two intentions in mind:

1. To explore Google's Guice Dependency Injection implementation, Dropwizard for web service Implementation, and Maven's capabilities to break a project down into modules
2. To use a password manager that I have built myself.

Status
======
As a desktop app, this is a functional app. The code is also broken down into modules. It's web service is not implemented.

Dependencies
============
Before the application may be used, we need to download and put two jars in $JAVA_HOME/jre/lib/security. To donwload the files, go to http://www.oracle.com/technetwork/java/javase/downloads/jce8-download-2133166.html. Here, you get jce_policy-8.zip. Unzip this file, and put local_policy.jar and US_export_policy.jar files in $JAVA_HME/jre/lib/security.
Other than that, this application uses Apache Derby for a database. No need to add any file for this database to be available as long as Java 8 is running on your system.

How to run this app
===================
The class MPMMainFrame in mpm-gui-desktop is the main class. Open that class using Eclipse IDE, and then press ctrl+f11 to run the main method in that class. That's it.
