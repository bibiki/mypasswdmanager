package com.gagi.passwd.manager;

import java.util.Arrays;

class Solution {
	
	public static void main(String[] args) {
		Solution s = new Solution();
		System.out.println(s.solution(new int[]{5, 2, 4, 6, 3, 7}));
		System.out.println(s.solution(new int[]{5, 12, 24, 36, 43, 57}));
		System.out.println(s.solution(new int[]{5, 10, 15, 25, 30, 7}));
	}
	
    public int solution(int[] A) {
        // write your code in Java SE 8
        ValueToIndex[] valuesToIndices = new ValueToIndex[A.length - 2];
        for(int i = 1; i < A.length - 1; i++) {
        	valuesToIndices[i - 1] = new ValueToIndex(i, A[i]);
        }//O(n) time complexity

        Arrays.sort(valuesToIndices);
        ValueToIndex secondWeakestLink = null;
        for(int i = 0; i < valuesToIndices.length; i++) {
        	if(Math.abs(valuesToIndices[i].index - valuesToIndices[0].index) > 1) {
        		secondWeakestLink = valuesToIndices[i];
        		break;
        	}
        }
        return valuesToIndices[0].value + secondWeakestLink.value;//but first and third may never be adjecant elements in A

    }
    
    private class ValueToIndex implements Comparable<ValueToIndex> {
        private final int index;
        private final int value;
        
        private ValueToIndex(int index, int value) {
            this.index = index;
            this.value = value;
        }
        
        @Override
        public int compareTo(ValueToIndex o) {
        	// TODO Auto-generated method stub
        	return new Integer(this.value).compareTo(new Integer(o.value));
        }
    }
}