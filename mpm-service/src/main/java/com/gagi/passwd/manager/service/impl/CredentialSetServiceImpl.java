package com.gagi.passwd.manager.service.impl;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.Set;

import javax.inject.Inject;

import com.gagi.passwd.manager.dao.CredentialSetDaoInt;
import com.gagi.passwd.manager.domain.CredentialSet;
import com.gagi.passwd.manager.service.CredentialSetServiceInt;

public class CredentialSetServiceImpl implements CredentialSetServiceInt {

	@Inject
	private CredentialSetDaoInt credentialSetDaoInt;
	
	public CredentialSet save(CredentialSet credentialSet) {
		return credentialSetDaoInt.save(credentialSet);
	}
	
	public CredentialSet update(CredentialSet credentialSet) {
		return credentialSetDaoInt.update(credentialSet);
	}
	
	public CredentialSet getById(long id) {
		return credentialSetDaoInt.getById(id);
	}
	
	public void delete(CredentialSet credentialSet) {
		credentialSetDaoInt.delete(credentialSet);
	}
	
	@Override
	public void delete(long id) {
		CredentialSet credentialSet = credentialSetDaoInt.getById(id);
		credentialSetDaoInt.delete(credentialSet);
	}
	
	@Override
	public Set<CredentialSet> getAll() {
		return credentialSetDaoInt.getAll();
	}
	
	@Override
	public void putOnClipboard(String message) {
		StringSelection selection = new StringSelection(message);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(selection, selection);
	}
	
	@Override
	public void scheduleClearingClipboard(long millis) {
		new Thread(() -> {
			try {
				Thread.sleep(millis);
			} catch (Exception e) {
				e.printStackTrace();
			}
			putOnClipboard("");
		}, "Overwriting clipboard").start();
	}
}
