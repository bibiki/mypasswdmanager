package com.gagi.passwd.manager.service;

import java.util.Set;

import com.gagi.passwd.manager.domain.CredentialSet;

public interface CredentialSetServiceInt {

	CredentialSet save(CredentialSet credentialSet);
	
	void delete(CredentialSet credentialSet);
	
	void delete(long id);
	
	CredentialSet getById(long id);
	
	CredentialSet update(CredentialSet credentialSet);
	
	Set<CredentialSet> getAll();
	
	void putOnClipboard(String message);
	
	void scheduleClearingClipboard(long millis);
}
