package com.gagi.passwd.manager.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author ngadhnjim
 *
 */
@Path("/hello")
public class HelloResource {

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getGreeting() {
		return "Hello, world!";
	}
	
	@GET
	@Path("path_param/{name}")
	@Produces(MediaType.TEXT_PLAIN)
	public String getNamedGreeting(@PathParam(value = "name") String name) {
		return "Hello, " + name;
	}
}
