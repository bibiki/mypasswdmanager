package com.gagi.passwd.manager;

import com.gagi.passwd.manager.resources.HelloResource;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class MPMWebServiceApplication extends Application<MPMWebServiceConfiguration> {

    public static void main(final String[] args) throws Exception {
        new MPMWebServiceApplication().run(args);
    }

    @Override
    public String getName() {
        return "mpm-web-web-service";
    }

    @Override
    public void initialize(final Bootstrap<MPMWebServiceConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final MPMWebServiceConfiguration configuration,
                    final Environment environment) {
    	environment.jersey().register(new HelloResource());
    }

}
