package com.gagi.passwd.manager.panels;

import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;



import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTable;



import com.gagi.passwd.manager.domain.CredentialSet;
import com.gagi.passwd.manager.service.CredentialSetServiceInt;
import com.gagi.passwd.manager.service.EncryptionDecryptionServiceInt;import com.gagi.passwd.manager.tables.MPMTableModel;
import com.google.common.base.Strings;


public class ActionsPanel extends JPanel {

	private final JButton copyToClipboard = new JButton();
	private final JButton removeThisCredentialSet = new JButton();
	private final JTable passwordsInDB;
	private final CredentialSetServiceInt credentialSetServiceInt;
	private final EncryptionDecryptionServiceInt encryptor;
	
	@Inject
	public ActionsPanel(JTable passwordsInDB, CredentialSetServiceInt credentialSetServiceInt, EncryptionDecryptionServiceInt encryptor) {
		this.credentialSetServiceInt = credentialSetServiceInt;
		this.encryptor = encryptor;
		this.passwordsInDB = passwordsInDB;
		setLayout(new GridBagLayout());
		try {
			Image clipboard = ImageIO.read(getClass().getClassLoader().getResource("clipboard-icon.png"));
			Image delete = ImageIO.read(getClass().getClassLoader().getResource("delete-icon.png"));
			copyToClipboard.setIcon(new ImageIcon(clipboard));
			removeThisCredentialSet.setIcon(new ImageIcon(delete));
			add(copyToClipboard);
			add(removeThisCredentialSet);
		  } catch (IOException ex) {
			  ex.printStackTrace();
		  }
	}
	
	public void addListeners() {
		copyToClipboard.addActionListener(l -> {
			if(passwordsInDB.getSelectedRow() < 0) {
				JOptionPane.showMessageDialog(null, "You should select a row first!");
				return;
			}
			JPasswordField encryptionKey = new JPasswordField(15);
			int okCxl = 0;

			String key = null;
			while(okCxl != JOptionPane.CANCEL_OPTION && Strings.isNullOrEmpty(key)) {
				okCxl = JOptionPane.showConfirmDialog(null, encryptionKey, "Enter Password", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
				key = new String(encryptionKey.getPassword());
			}
			if(okCxl == JOptionPane.OK_OPTION) {
				long id = (long)passwordsInDB.getModel().getValueAt(passwordsInDB.getSelectedRow(), -1);
				CredentialSet credentialSet = credentialSetServiceInt.getById(id);
				String encryptedPassword = credentialSet.getEncryptedPassword();
				try {
					String decryptedPassword = encryptor.decrypt(encryptedPassword, key);
					credentialSetServiceInt.putOnClipboard(decryptedPassword);
					credentialSetServiceInt.scheduleClearingClipboard(15000l);
				}
				catch(RuntimeException re) {
					JOptionPane.showMessageDialog(null, re.getMessage());
				}
			}
		});
		
		removeThisCredentialSet.addActionListener(l -> {
			if(passwordsInDB.getSelectedRow() < 0) {
				JOptionPane.showMessageDialog(null, "You should select a row first!");
				return;
			}
			int okCxl = JOptionPane.showConfirmDialog(null, "This will delete your credentials from the database. You will not be able to retreive them ever again.", "Confirm removal", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
			if(JOptionPane.OK_OPTION == okCxl) {
				long id = (long)passwordsInDB.getModel().getValueAt(passwordsInDB.getSelectedRow(), -1);
				credentialSetServiceInt.delete(id);
				((MPMTableModel)this.passwordsInDB.getModel()).setCredentialSet(credentialSetServiceInt.getAll());
			}
		});
	}
}
