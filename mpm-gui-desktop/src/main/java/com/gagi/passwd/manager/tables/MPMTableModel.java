package com.gagi.passwd.manager.tables;

import java.util.Arrays;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.table.AbstractTableModel;

import com.gagi.passwd.manager.domain.CredentialSet;

public class MPMTableModel extends AbstractTableModel {

	private static final String[] COLUMN_NAMES = new String[]{"Target app", "Description"};
	private CredentialSet[] credentialSet;
	
	public MPMTableModel(Set<CredentialSet> credentialSet) {
		this.credentialSet = credentialSet.toArray(new CredentialSet[credentialSet.size()]);
	}
	
	@Override
	public int getColumnCount() {
		return COLUMN_NAMES.length;
	}
	
	@Override
	public int getRowCount() {
		return credentialSet.length;
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if(0 == columnIndex) {
			return this.credentialSet[rowIndex].getTargetApp();
		}
		else if (1 == columnIndex) {
			return this.credentialSet[rowIndex].getAppDescription();
		}
		else if(2 == columnIndex) {
			
		}
		else if(-1 == columnIndex) {
			return this.credentialSet[rowIndex].getId();
		}
		return "";
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return columnIndex == 2;
	}
	
	@Override
	public String getColumnName(int column) {
		return COLUMN_NAMES[column];
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		System.out.println("editing: " + aValue);
	}
	
	public void setCredentialSet(Set<CredentialSet> credentialSets) {
		this.credentialSet = credentialSets.toArray(new CredentialSet[credentialSets.size()]);
		this.fireTableDataChanged();
	}
}
