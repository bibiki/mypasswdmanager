package com.gagi.passwd.manager.config;

import javax.swing.JPanel;

import com.gagi.passwd.manager.dao.CredentialSetDaoInt;
import com.gagi.passwd.manager.dao.impl.CredentialSetDaoImpl;
import com.gagi.passwd.manager.panels.ActionsPanel;
import com.gagi.passwd.manager.panels.MPMMainPanel;
import com.gagi.passwd.manager.service.CredentialSetServiceInt;
import com.gagi.passwd.manager.service.EncryptionDecryptionServiceInt;
import com.gagi.passwd.manager.service.impl.CredentialSetServiceImpl;
import com.gagi.passwd.manager.service.impl.EncryptionDecryptionAES;
import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

public class GuiceConfiguration extends AbstractModule {

	@Override
	protected void configure() {
		bind(JPanel.class).annotatedWith(Names.named("mainPanel")).to(MPMMainPanel.class);
		bind(JPanel.class).annotatedWith(Names.named("actionsPanel")).to(ActionsPanel.class);
		bind(EncryptionDecryptionServiceInt.class).to(EncryptionDecryptionAES.class);
		bind(CredentialSetServiceInt.class).to(CredentialSetServiceImpl.class);
		bind(CredentialSetDaoInt.class).to(CredentialSetDaoImpl.class);
	}
}
