package com.gagi.passwd.manager.window;

import javax.inject.Inject;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.gagi.passwd.manager.config.GuiceConfiguration;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Named;

public class MPMMainFrame extends JFrame {

	private static final long serialVersionUID = -2837611333733254395L;

	@Inject
	public MPMMainFrame(@Named("mainPanel") JPanel mainPanel) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		add(mainPanel);
		pack();
		setResizable(false);
		setVisible(true);
	}
	
	public static void main(String[] args) {
		Injector injector = Guice.createInjector(new GuiceConfiguration());
		JFrame mainFrame = injector.getInstance(MPMMainFrame.class);
	}
}
